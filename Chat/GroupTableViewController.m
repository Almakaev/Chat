//
//  GroupTableViewController.m
//  Chat
//
//  Created by almakaev iliyas on 29.06.15.
//  Copyright (c) 2015 almakaev iliyas. All rights reserved.
//

#import "GroupTableViewController.h"
#import "CompanionsViewController.h"
#import "ChatService.h"
#import "GroupTableViewCell.h"
#import "DialogViewController.h"

@interface GroupTableViewController () <QBChatDelegate>

@property (nonatomic, strong) NSMutableArray *users;
@property (nonatomic, strong) NSMutableArray *dialogs;
@property (nonatomic, strong) NSMutableArray *selectedUsers;
@property (nonatomic, strong) QBUUser *recipient;

@end

@implementation GroupTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Групповой чат";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Назад"
                                                                            style:UIBarButtonItemStylePlain
                                                                           target:self
                                                                           action:@selector(back)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                                           target:self
                                                                                           action:@selector(add)];
    [self.tableView registerNib:[UINib nibWithNibName:@"GroupTableViewCell" bundle:nil] forCellReuseIdentifier:@"idGroup"];
    
    self.users = [NSMutableArray array];
    self.dialogs = [NSMutableArray array];
    self.selectedUsers = [NSMutableArray array];
    
    for (QBChatDialog *chatDialog in [ChatService shared].dialogs)
    {
        if (chatDialog.type == QBChatDialogTypePrivate) {
            QBUUser *recipient = [ChatService shared].usersAsDictionary[@(chatDialog.recipientID)];

            NSLog(@"User show = %@", recipient.login);
            [self.users addObject:recipient];
        }
    }
    NSLog(@"array users = %@", self.users);
 /*   QBChatDialog *Dialog;
    
    for (recipient in self.dialogs)
    {
        NSLog(@"User show = %@", recipient.login);
    }*/
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.users count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GroupTableViewCell *cell = (GroupTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"idGroup"];

    QBUUser *recipient = self.users[indexPath.row];
    cell.tag  = indexPath.row;
    cell.nameUserLabel.text = recipient.login;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    
    QBUUser *user = self.users[indexPath.row];
    if([self.selectedUsers containsObject:user]){
        [self.selectedUsers removeObject:user];
        selectedCell.accessoryType = UITableViewCellAccessoryNone;
    }else{
        [self.selectedUsers addObject:user];
        selectedCell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
}

// Exit
- (void)back
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Add group
- (void)add
{
    if(self.selectedUsers.count == 0){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Внимание"
                                                        message:@"Выберите контакты для создания группового чата"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    
    QBChatDialog *chatDialog = [QBChatDialog new];
    
    NSMutableArray *selectedUsersIDs = [NSMutableArray array];
    NSMutableArray *selectedUsersNames = [NSMutableArray array];
    for(QBUUser *user in self.selectedUsers){
        [selectedUsersIDs addObject:@(user.ID)];
        [selectedUsersNames addObject:user.login];
    }
    chatDialog.occupantIDs = selectedUsersIDs;
    
    if(self.selectedUsers.count == 1){
        chatDialog.type = QBChatDialogTypePrivate;
    }else{
        chatDialog.name = [selectedUsersNames componentsJoinedByString:@","];
        chatDialog.type = QBChatDialogTypeGroup;
    }
    
    [QBRequest createDialog:chatDialog successBlock:^(QBResponse *response, QBChatDialog *createdDialog) {
        
        // save dialog to cache
        //
        if(createdDialog.type != QBChatDialogTypePrivate || [ChatService shared].dialogsAsDictionary[createdDialog.ID] == nil){
            [[ChatService shared].dialogs insertObject:createdDialog atIndex:0];
            [[ChatService shared].dialogsAsDictionary setObject:createdDialog forKey:createdDialog.ID];
        }else{
            createdDialog = [ChatService shared].dialogsAsDictionary[createdDialog.ID];
        }
        
        // and join it
        if(createdDialog.type != QBChatDialogTypePrivate){
            [createdDialog setOnJoin:^() {
                NSLog(@"Dialog joined");
                
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationGroupDialogJoined object:nil];
            }];
            [createdDialog setOnJoinFailed:^(NSError *error) {
                NSLog(@"Join Fail, error: %@", error);
            }];
            [createdDialog join];
        }
    //    CompanionsViewController *companionsViewController = [[CompanionsViewController alloc] init];
        DialogViewController *dialogViewController = [[DialogViewController alloc] init];
        dialogViewController.dialogUsers = createdDialog;
        UINavigationController *componentNC = [[UINavigationController alloc] initWithRootViewController:dialogViewController];
        componentNC.navigationBar.translucent = NO;
    //    [weakSelf.navigationController popViewControllerAnimated:YES];
        [self back];
        /*[self presentViewController:componentNC
                           animated:YES
                         completion:nil];*/
        
    } errorBlock:^(QBResponse *response) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Errors"
                                                        message:response.error.error.localizedDescription
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
        [alert show];
        
    }];

}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
