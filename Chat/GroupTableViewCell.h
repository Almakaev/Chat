//
//  GroupTableViewCell.h
//  Chat
//
//  Created by almakaev iliyas on 29.06.15.
//  Copyright (c) 2015 almakaev iliyas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameUserLabel;

@end
