//
//  LoginViewController.m
//  Chat
//
//  Created by almakaev iliyas on 16.06.15.
//  Copyright (c) 2015 almakaev iliyas. All rights reserved.
//

#import "LoginViewController.h"
#import "ProfileViewController.h"
#import "CompanionsViewController.h"
#import <Quickblox/Quickblox.h>
#import "ChatService.h"

@interface LoginViewController () <UITextFieldDelegate>

- (IBAction)registration:(id)sender;
- (IBAction)logON:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *loginField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;

@property (strong, nonatomic) UITabBarController *tabController;
@property (strong, nonatomic) CompanionsViewController *companionsVC;

@end

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.companionsVC = [[CompanionsViewController alloc] init];
    self.loginField.delegate = self;
    self.passwordField.delegate = self;
}

- (IBAction)registration:(id)sender
{
    // Create session request
    [QBRequest createSessionWithSuccessBlock:^(QBResponse *response, QBASession *session) {
        //Your Quickblox session was created successfully
        // Save current user
        //
        QBUUser *currentUser = [QBUUser user];
        currentUser.ID = session.userID;
        
        currentUser.login = self.loginField.text;
        currentUser.password = self.passwordField.text;
        
        [QBRequest signUp:currentUser successBlock:^(QBResponse *response, QBUUser *user) {
            [self connect];
        } errorBlock:^(QBResponse *response) {
            NSString *errorMessage = [[response.error description] stringByReplacingOccurrencesOfString:@"(" withString:@""];
            errorMessage = [errorMessage stringByReplacingOccurrencesOfString:@")" withString:@""];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Errors"
                                                            message:errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles: nil];
            [alert show];

        }];

    } errorBlock:^(QBResponse *response) {
        NSString *errorMessage = [[response.error description] stringByReplacingOccurrencesOfString:@"(" withString:@""];
        errorMessage = [errorMessage stringByReplacingOccurrencesOfString:@")" withString:@""];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Errors"
                                                        message:errorMessage
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
        [alert show];

    }];
}

- (void)connect
{
    // QuickBlox session creation
    QBSessionParameters *extendedAuthRequest = [[QBSessionParameters alloc] init];
    
    extendedAuthRequest.userLogin = self.loginField.text;
    extendedAuthRequest.userPassword = self.passwordField.text;
    
    
    //
    [QBRequest createSessionWithExtendedParameters:extendedAuthRequest successBlock:^(QBResponse *response, QBASession *session) {
        
        // Save current user
        //
        QBUUser *currentUser = [QBUUser user];
        currentUser.ID = session.userID;
        
        currentUser.login = self.loginField.text;
        currentUser.password = self.passwordField.text;
        
        // Login to QuickBlox Chat
        //
        [[ChatService shared] loginWithUser:currentUser completionBlock:^{
        }];
        
        [self enterProfile];
        
    } errorBlock:^(QBResponse *response) {
        
        NSString *errorMessage = [[response.error description] stringByReplacingOccurrencesOfString:@"(" withString:@""];
        errorMessage = [errorMessage stringByReplacingOccurrencesOfString:@")" withString:@""];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Errors"
                                                        message:errorMessage
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
        [alert show];
    }];

    
}

#pragma mark
#pragma mark UITextFieldDelegate

// Hide Keyboard
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self hideKeyboard];
}

/*- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self hideKeyboard];
}*/

- (void) hideKeyboard
{
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self hideKeyboard];
    return NO;
}

- (IBAction)logON:(id)sender
{
    [self connect];
}

- (void)enterProfile
{
    
    UINavigationController *companionsNC = [[UINavigationController alloc] initWithRootViewController:self.companionsVC];
    companionsNC.navigationBar.translucent = NO;
 //   companionsVC.tabBarItem.title = @"Собеседники";
    
    ProfileViewController *profileVC = [[ProfileViewController alloc] init];
    UINavigationController *profileNC = [[UINavigationController alloc] initWithRootViewController:profileVC];
    profileNC.tabBarItem.title = @"Профиль";
    
    /*   self.tabController = [[UITabBarController alloc] init];
    self.tabController.viewControllers = @[companionsVC, profileNC];*/
    
    [self presentViewController:companionsNC
                       animated:YES
                     completion:nil];
}

@end
